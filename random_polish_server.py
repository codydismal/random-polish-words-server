from flask import Flask
from getters import getter

app = Flask(__name__)


@app.route('/')
def hello_world():
    return str(getter.get_word_and_description())


if __name__ == '__main__':
    app.run()
