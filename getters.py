import requests
from bs4 import BeautifulSoup, Tag


class InfoGetter:
    def __init__(self):
        self.words_addr = "http://losuje.pl/php/getwords.php?categories=[%22cechy%20charakteru%22,%22emocje%20i%20uczucia%22,%22frazy%20przymiotnikowe%22,%22homonimy%22,%22owoce%22,%22pogoda%22,%22popularne%20rzeczowniki%22,%22przymiotniki%22,%22rzeczowniki%22,%22sport%22,%22warzywa%22,%22zawody%22,%22zwierz%C4%99ta%22,%22przys%C5%82owia%20i%20powiedzenia%22]&length=&number=1"
        self.description_addr = "https://sjp.pl/"

    def _get_random_word(self):
        word_request = requests.get(self.words_addr)
        return word_request.json()[0].get('tresc').upper()

    def _get_word_description(self, word):
        page = requests.get("{0}{1}".format(self.description_addr, word)).text
        soup = BeautifulSoup(page, 'html.parser')
        description = soup.find('p', attrs={'style': "margin: .5em 0; font: medium/1.4 sans-serif; max-width: 32em; "})
        try:
            description = description.contents
        except AttributeError:
            return "Ten opis jeszcze nie jest gotowy"
        result = '\n'.join([d for d in description if type(d) is not Tag]).upper()
        return result

    def get_word_and_description(self):
        word = self._get_random_word()
        result = {
            'word': word,
            'description': self._get_word_description(word)
        }
        return result


getter = InfoGetter()
